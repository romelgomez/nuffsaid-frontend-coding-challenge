import { render } from '@testing-library/react';
import { messages$, turnOff } from '../Api';

import App from '../App';

test('renders learn react link', () => {
  const comp = render(<App />);
  expect(comp).toBeTruthy();
});

test('Start emitting after call turn Off function', (done) => {
  messages$.subscribe((m) => {
    expect(m.message.length).toBeGreaterThan(0);

    done();
  });

  turnOff(false);
});
