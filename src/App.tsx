import React, { useState } from 'react';
import { useEffect } from 'react';
import { Message, messages$, Priority, turnOff } from './Api';
import Grid from '@mui/material/Grid';
import Paper from '@mui/material/Paper';
import { Snackbar } from '@mui/material';
import MuiAlert, { AlertProps } from '@mui/material/Alert';

const Alert = React.forwardRef<HTMLDivElement, AlertProps>(function Alert(
  props,
  ref
) {
  return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
});

interface AlertMessage {
  open: boolean;
  m: string;
}

const clearMessage = (mList: Message[], id: string): Message[] => {
  return mList.filter((v) => {
    return v.id !== id;
  });
};

interface MessageProps {
  m: Message;
  priority: Priority;
  clear: () => void;
}

export const MessageElement: React.FC<MessageProps> = ({
  m,
  priority,
  clear,
}) => {
  const [visible, setVisible] = useState<boolean>(true);

  let backgroundColor: string = '#F56236';

  if (priority === Priority.Info) {
    backgroundColor = '#88FCA3';
  } else if (priority === Priority.Warn) {
    backgroundColor = '#FCE788';
  }

  if (visible) {
    return (
      <Paper
        key={m?.id}
        style={{
          backgroundColor: backgroundColor,
        }}
        className={'paper-container'}
      >
        <div className={'message'}>{m?.message}</div>

        <div className={'actions-container'}>
          <span
            className={'clear'}
            onClick={() => {
              setVisible(false);
              clear();
            }}
          >
            Clear
          </span>
        </div>
      </Paper>
    );
  }

  return <></>;
};

const App: React.FC<{}> = () => {
  const [messagesSubscription, setMessagesSubscription] =
    useState<boolean>(false);

  const [errorMessages, setErrorMessages] = useState<Message[]>([]);
  const [warningMessages, setWarningMessages] = useState<Message[]>([]);
  const [infoMessages, setInfoMessages] = useState<Message[]>([]);

  const [open, setOpen] = React.useState<AlertMessage>({
    open: false,
    m: '',
  });

  const handleClose = (event?: React.SyntheticEvent, reason?: string) => {
    if (reason === 'clickaway') {
      return;
    }

    setOpen({ open: false, m: '' });
  };

  useEffect(() => {
    const sub = messages$.subscribe((v) => {
      if (v.priority === 0) {
        setOpen({ open: true, m: `${v.message}` });
      }

      const errorList =
        v.priority === Priority.Error ? [v, ...errorMessages] : errorMessages;
      const warningList =
        v.priority === Priority.Warn
          ? [v, ...warningMessages]
          : warningMessages;
      const infoList =
        v.priority === Priority.Info ? [v, ...infoMessages] : infoMessages;

      setErrorMessages([...errorList]);
      setWarningMessages([...warningList]);
      setInfoMessages([...infoList]);
    });

    turnOff(messagesSubscription);

    return () => {
      sub.unsubscribe();
    };
  }, [messagesSubscription, errorMessages, warningMessages, infoMessages]);

  return (
    <div>
      <div>
        <h3 className={'test-title'}>nuffsaid.com Coding Challenge</h3>

        <hr />
      </div>
      <div className={'btn-container'}>
        <button
          className={'btn'}
          onClick={() => {
            turnOff(!messagesSubscription);
            setMessagesSubscription(!messagesSubscription);
          }}
        >
          {messagesSubscription ? 'start' : 'stop'}
        </button>{' '}
        <button
          className={'btn'}
          onClick={() => {
            setErrorMessages([]);
            setWarningMessages([]);
            setInfoMessages([]);
          }}
        >
          clear
        </button>
      </div>
      <div>
        <Grid
          sx={{ flexGrow: 1 }}
          container
          spacing={2}
          direction="row"
          justifyContent="center"
          alignItems="flex-start"
        >
          <Grid item xs={4} className={'grid-iten'}>
            <div className={'title-container'}>
              <div>
                <h4 className={'title'}>Error Type 1</h4>
              </div>
              <div>Count: {errorMessages.length}</div>
            </div>
            <div>
              {errorMessages?.map?.((m, i) => (
                <MessageElement
                  key={i}
                  priority={m.priority}
                  m={m}
                  clear={() => {
                    setErrorMessages((oldMessages) => [
                      ...clearMessage(oldMessages, m.id),
                    ]);
                  }}
                />
              ))}
            </div>
          </Grid>
          <Grid item xs={4} className={'grid-iten'}>
            <div className={'title-container'}>
              <div>
                <h4 className={'title'}>Warning Type 2</h4>
              </div>
              <div>Count: {warningMessages.length}</div>
            </div>

            <div>
              {warningMessages?.map?.((m, i) => (
                <MessageElement
                  key={i}
                  priority={m.priority}
                  m={m}
                  clear={() => {
                    setWarningMessages((oldMessages) => [
                      ...clearMessage(oldMessages, m.id),
                    ]);
                  }}
                />
              ))}
            </div>
          </Grid>
          <Grid item xs={4} className={'grid-iten'}>
            <div className={'title-container'}>
              <div>
                <h4 className={'title'}>Info Type 3</h4>
              </div>
              <div>Count: {infoMessages.length}</div>
            </div>

            <div>
              {infoMessages?.map?.((m, i) => (
                <MessageElement
                  key={i}
                  priority={m.priority}
                  m={m}
                  clear={() => {
                    setInfoMessages((oldMessages) => [
                      ...clearMessage(oldMessages, m.id),
                    ]);
                  }}
                />
              ))}
            </div>
          </Grid>
        </Grid>
      </div>
      <Snackbar
        anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
        open={open.open}
        autoHideDuration={2000}
        onClose={handleClose}
      >
        <Alert onClose={handleClose} severity="error" sx={{ width: '100%' }}>
          {open.m}!
        </Alert>
      </Snackbar>
    </div>
  );
};

export default App;
