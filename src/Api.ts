import { random } from 'lodash';
import { lorem } from 'faker';
import {
  of,
  MonoTypeOperatorFunction,
  SchedulerLike,
  Subject,
  interval,
  merge,
} from 'rxjs';
import {
  delay,
  concatMap,
  switchMap,
  filter,
  bufferToggle,
  mergeMap,
  distinctUntilChanged,
  share,
  windowToggle,
} from 'rxjs/operators';
import { v4 as uuidv4 } from 'uuid';

export type NumberGenerator = (min: number, max: number) => number;

export enum Priority {
  Error,
  Warn,
  Info,
}

export interface Message {
  message: string;
  priority: Priority;
  id: string;
}

const message = (): Message => {
  const sentence = lorem.sentence();
  const priority = random(0, 2) as Priority;
  return { message: sentence, priority, id: uuidv4() };
};

const pauseSubj$ = new Subject<boolean>();
const pause$ = pauseSubj$.pipe(distinctUntilChanged(), share());

const on$ = pause$.pipe(filter((v: boolean) => !v));
const off$ = pause$.pipe(filter((v: boolean) => v));

const randomDelay = <T>(
  min: number,
  max: number,
  generator: NumberGenerator = random,
  scheduler?: SchedulerLike
): MonoTypeOperatorFunction<T> =>
  concatMap((value) => of(value).pipe(delay(generator(min, max), scheduler)));

const mensageGenerator$ = interval(500).pipe(
  randomDelay(1000, 3000),
  switchMap(() => {
    return of(message());
  })
);

const source$ = mensageGenerator$;

export const messages$ = merge(
  source$.pipe(
    bufferToggle(off$, () => on$),
    mergeMap((x) => x)
  ),
  source$.pipe(
    windowToggle(on$, () => off$),
    mergeMap((x) => x)
  )
);

export const turnOff = (pause: boolean) => {
  if (pause) {
    pauseSubj$.next(true);
  } else {
    pauseSubj$.next(false);
  }
};

export default messages$;
